<?php

namespace Drupal\pluggable_entity_view_builder;

/**
 * Helper methods to wrap output in a wrapper and container classes.
 */
trait ElementWrapTrait {

  /**
   * Wrap a component with a container class, and some identifier class.
   *
   * @param array $element
   *   The render array of the element which is the component.
   * @param string $wrapper_classes
   *   The class name to wrap the element with. The identifier class is mainly
   *   used to distinguish the component on the phpunit tests.
   *   Furthermore, you may add container classes.
   *   Best practice would be avoiding adding any styling css beyond a
   *   container, as those should be inside a Twig file.
   *
   * @return array
   *   Render array.
   */
  protected function wrapElementWithContainer(array $element, string $wrapper_classes): array {
    $build = [];
    $build['container'] = [
      '#type' => 'container',
    ];

    $build['container']['#attributes']['class'][] = $wrapper_classes;
    $build['container']['element'] = $element;

    return $build;
  }

}
